Analysis and relevant data for the UNDP GCF proposal on Broadening Effective Use of Climate Information Products and Services for Increased Resilience and Adaptation to Climate Change in Uganda.
The work presented here was commissioned by UNDP.
